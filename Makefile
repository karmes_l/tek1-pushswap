##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Thu Dec 11 16:20:54 2014 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	=

NAME	= push_swap

SRCS	= main.c \
	pushswap.c \
	init_list.c \
	print_list.c \
	algo_sort_list.c \
	fct_sort_list.c \
	fct_sort_list2.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	make -C lib
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) -L./lib/ -lmy

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
