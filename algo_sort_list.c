/*
** sort_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec 10 13:00:06 2014 lionel karmes
** Last update Sun Dec 14 17:29:40 2014 lionel karmes
*/

#include "my.h"

int		list_sorted(t_list *list)
{
  t_element	*tmp;

  tmp = list->l_start;
  while (tmp != NULL)
    {
      if (tmp->e_next != NULL && tmp->e_data > tmp->e_next->e_data)
      	return (0);
      tmp = tmp->e_next;
    }
  return (1);
}

void		put_sort_in_list_b(t_list **list_a, t_list **list_b, int option_v)
{
  pb_pa(list_a, list_b, 2);
  if ((*list_b)->len > 1 &&
      (*list_b)->l_start->e_data < (*list_b)->l_start->e_next->e_data)
    {
      if (option_v)
	print_lists(*list_a, *list_b);
      if ((*list_a)->len > 1
	  && (*list_a)->l_start->e_data > (*list_a)->l_start->e_next->e_data)
	ss(list_a, list_b);
      else
	sa_sb(list_b, 2);
    }
}

void		sort_list_step1(t_list **list_a, t_list **list_b, int option_v)
{
  while (!list_sorted(*list_a))
    {
      if ((*list_a)->l_start->e_data > (*list_a)->l_start->e_next->e_data)
      	sa_sb(list_a, 1);
      else if ((*list_a)->l_start->e_data > (*list_a)->l_end->e_data)
      	rra_rrb(list_a, 1);
      else
	put_sort_in_list_b(list_a, list_b, option_v);
      if (option_v)
	print_lists(*list_a, *list_b);
    }
}

void		sort_list_step2(t_list **list_a, t_list **list_b, int option_v)
{
  if ((*list_a)->l_start->e_data > (*list_a)->l_end->e_data)
    {
      sa_sb(list_a, 1);
      if (option_v)
	print_lists(*list_a, *list_b);
    }
  while ((*list_b)->len > 0)
    {
      pb_pa(list_b, list_a, 1);
      if (option_v)
	print_lists(*list_a, *list_b);
      if ((*list_a)->l_start->e_data > (*list_a)->l_start->e_next->e_data)
	{
	  if ((*list_b)->len > 1 &&
	      (*list_b)->l_start->e_data < (*list_b)->l_start->e_next->e_data)
	    ss(list_a, list_b);
	  else
	    sa_sb(list_a, 1);
	  if (option_v)
	    print_lists(*list_a, *list_b);
	}
    }
}

void		sort_list(t_list **list_a, t_list **list_b, int option_v)
{
  while (!list_sorted(*list_a))
    {
      sort_list_step1(list_a, list_b, option_v);
      sort_list_step2(list_a, list_b, option_v);
    }
}
