/*
** sort_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec 10 13:00:06 2014 lionel karmes
** Last update Sun Dec 14 16:53:20 2014 lionel karmes
*/

#include "my.h"

void		sa_sb(t_list **list_1, int what_list)
{
  int		nb;

  nb = (*list_1)->l_start->e_data;
  (*list_1)->l_start->e_data = (*list_1)->l_start->e_next->e_data;
  (*list_1)->l_start->e_next->e_data = nb;
  if (what_list == 1)
    my_putstr("sa ");
  else if (what_list == 2)
    my_putstr("sb ");
}

void		ra_rb(t_list **list_1, int what_list)
{
  int		nb;
  t_element	*element;

  element = (*list_1)->l_start;
  nb = (*list_1)->l_start->e_data;
  (*list_1)->l_start = (*list_1)->l_start->e_next;
  (*list_1)->l_start->e_prev = NULL;
  (*list_1)->len--;
  my_put_in_list_end(list_1, nb);
  free(element);
  if (what_list == 1)
    my_putstr("ra ");
  else if (what_list == 2)
    my_putstr("rb ");
}

void		rra_rrb(t_list **list_1, int what_list)
{
  int		nb;
  t_element	*element;

  element = (*list_1)->l_end;
  nb = (*list_1)->l_end->e_data;
  (*list_1)->l_end = (*list_1)->l_end->e_prev;
  (*list_1)->l_end->e_next = NULL;
  (*list_1)->len--;
  my_put_in_list_start(list_1, nb);
  free(element);
  if (what_list == 1)
    my_putstr("rra ");
  else if (what_list == 2)
    my_putstr("rrb ");
}

void		pb_pa(t_list **list_1, t_list **list_2, int what_list)
{
  int		nb;
  t_element	*element;

  element = (*list_1)->l_start;
  nb = (*list_1)->l_start->e_data;
  (*list_1)->l_start = (*list_1)->l_start->e_next;
  if ((*list_1)->len > 1)
    (*list_1)->l_start->e_prev = NULL;
  (*list_1)->len--;
  my_put_in_list_start(list_2, nb);
  free(element);
  if (what_list == 1)
    my_putstr("pa ");
  else if (what_list == 2)
    my_putstr("pb ");
}
