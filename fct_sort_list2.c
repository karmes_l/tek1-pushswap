/*
** sort_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec 10 13:00:06 2014 lionel karmes
** Last update Sun Dec 14 17:15:38 2014 lionel karmes
*/

#include "my.h"

void		ss(t_list **list_a, t_list **list_b)
{
  sa_sb(list_a, 0);
  sa_sb(list_b, 0);
  my_putstr("ss ");
}

void		rr(t_list **list_a, t_list **list_b)
{
  ra_rb(list_a, 0);
  ra_rb(list_b, 0);
  my_putstr("rr ");
}

void		rrr(t_list **list_a, t_list **list_b)
{
  rra_rrb(list_a, 0);
  rra_rrb(list_b, 0);
  my_putstr("rrr ");
}
