/*
** list.h for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:49:38 2014 lionel karmes
** Last update Sun Dec 14 22:21:38 2014 lionel karmes
*/

#ifndef LIST_H_
# define LIST_H_

typedef struct		s_element
{
  int			e_data;
  struct s_element	*e_next;
  struct s_element	*e_prev;
}			t_element;

typedef struct		s_list
{
  int			len;
  t_element		*l_start;
  t_element		*l_end;
}			t_list;

#endif /* !LIST_H_ */
