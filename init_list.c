/*
** init_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:41:33 2014 lionel karmes
** Last update Sun Dec 14 17:30:54 2014 lionel karmes
*/

#include "my.h"

void		my_put_in_list_end(t_list **list, int nb)
{
  t_element	*element;

  if ((element = malloc(sizeof(t_element))) == NULL)
    exit(0);
  element->e_data = nb;
  element->e_next = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_prev = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      (*list)->l_end->e_next = element;
      element->e_prev = (*list)->l_end;
      (*list)->l_end = element;
    }
  (*list)->len++;
}

void		my_put_in_list_start(t_list **list, int nb)
{
  t_element	*element;

  if ((element = malloc(sizeof(t_element))) == NULL)
    exit(0);
  element->e_data = nb;
  element->e_prev = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_next = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      (*list)->l_start->e_prev = element;
      element->e_next = (*list)->l_start;
      (*list)->l_start = element;
    }
  (*list)->len++;
}

t_list		*new_list()
{
  t_list	*list;

  if ((list = malloc(sizeof(t_list))) == NULL)
    exit(0);
  list->len = 0;
  list->l_start = NULL;
  list->l_end = NULL;
  return (list);
}

void		remove_list(t_list **list)
{
  t_element	*tmp;
  t_element	*element;

  if (*list != NULL)
    {
      tmp = (*list)->l_start;
      	while (tmp != NULL)
      	  {
      	    element = tmp;
      	    tmp = tmp->e_next;
      	    free(element);
      	  }
      free(*list);
      *list = NULL;
    }
}

t_list		*init_list(int ac, char **av, int *option_v)
{
  t_list	*list;
  int		i;

  list = new_list();
  i = 1;
  while (i < ac)
    {
      if (my_str_isnum(av[i]))
	my_put_in_list_end(&list, my_getnbr(av[i]));
      else if (my_strcmp(av[i], "-v") == 0)
	*option_v = 1;
      i++;
    }
  return (list);
}
