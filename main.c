/*
** main.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:39:14 2014 lionel karmes
** Last update Tue Dec  9 14:53:32 2014 lionel karmes
*/

#include "my.h"

int	main(int ac, char **av)
{
  pushswap(ac, av);
  return (0);
}
