/*
** print_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec 10 12:51:11 2014 lionel karmes
** Last update Sun Dec 14 17:18:32 2014 lionel karmes
*/

#include "my.h"

void		print_list(t_list *list)
{
  t_element	*tmp;

  tmp = list->l_start;
  while (tmp != NULL)
    {
      my_putnbr(tmp->e_data);
      my_putchar(' ');
      tmp = tmp->e_next;
    }
}

void		print_lists(t_list *list_a, t_list *list_b)
{

  my_putstr("\nList a : ");
  print_list(list_a);
  my_putstr("\nList b : ");
  print_list(list_b);
  my_putchar('\n');
}
