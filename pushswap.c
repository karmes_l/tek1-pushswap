/*
** pushswap.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:40:11 2014 lionel karmes
** Last update Sun Dec 14 16:05:01 2014 lionel karmes
*/

#include "my.h"

void		pushswap(int ac, char **av)
{
  t_list	*list_a;
  t_list	*list_b;
  int		option_v;

  option_v = 0;
  list_a = init_list(ac, av, &option_v);
  list_b = new_list();
  if (list_a->len > 1)
    sort_list(&list_a, &list_b, option_v);
  remove_list(&list_a);
  remove_list(&list_b);
}
